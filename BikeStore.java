//Shay Alex Lelichev 2043812
public class BikeStore {
    
    
    public static void main(String args[]) {

        //manufacturer, gears, max speed
        Bicycle[] bicycleArray = new Bicycle[4];
        bicycleArray[0] = new Bicycle("CCM", 10, 30);
        bicycleArray[1] = new Bicycle("DOREL", 15, 37);
        bicycleArray[2] = new Bicycle("NORCO", 12, 32);
        bicycleArray[3] = new Bicycle("BMX", 17, 26);

        for(int i = 0; i < bicycleArray.length; i++) {

            System.out.println(bicycleArray[i]);
        }
    }
}
