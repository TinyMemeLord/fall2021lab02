//Shay Alex Lelichev 2043812
public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;  
    
    public Bicycle(String setManufacturer , int setNumberGears , double setMaxSpeed) {

        this.manufacturer = setManufacturer;
        this.numberGears = setNumberGears;
        this.maxSpeed = setMaxSpeed;
    }

    public String getManufacturer() {

        return this.manufacturer;
    }

    public int getNumberGears() {

        return this.numberGears;
    }

    public double getMaxSpeed() {

        return this.maxSpeed;
    }

    public String toString() {

        return "Manufacturer: " + this.manufacturer + " , Number of Gears: " + this.numberGears + " , MaxSpeed: " + this.maxSpeed;
    }
}
